module gitlab.com/gitlab-org/security-products/analyzers/npm-audit

require (
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.1
	github.com/urfave/cli/v2 v2.4.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.7.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.24.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v3 v3.12.0
)

require (
	github.com/Microsoft/go-winio v0.5.2 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20220407094043-a94812496cf5 // indirect
	github.com/emirpasic/gods v1.15.0 // indirect
	golang.org/x/crypto v0.0.0-20220511200225-c6db032c6c88 // indirect
	golang.org/x/net v0.0.0-20220412020605-290c469a71a5 // indirect
	golang.org/x/sys v0.0.0-20220412071739-889880a91fd5 // indirect
)

go 1.15
