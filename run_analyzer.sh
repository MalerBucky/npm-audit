#!/bin/sh

find "${CI_PROJECT_DIR}/qa/fixtures" -mindepth 1 -maxdepth 1 -type d | while read -r project; do
    REPORT_PATH="$project/$REPORT_FILENAME"
    DIRNAME="$(basename $project)"
    TOOL="$(echo $DIRNAME | cut -d '-' -f 1)"
    EXPECTATION="${CI_PROJECT_DIR}/test/expect/${DIRNAME}.txt"

    echo "running on $project with $TOOL audit"

    docker run --volume "$project:/tmp/project" \
        --env CI_PROJECT_DIR="/tmp/project" \
        --env TOOL="$TOOL" "$TMP_IMAGE" /analyzer run

    echo "writing ${DIRNAME}/${REPORT_FILENAME}"
done

exit 0
