package main

import (
	"testing"

	"github.com/stretchr/testify/require"

	issue "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

func TestParseSeverity(t *testing.T) {

	for _, tc := range []struct {
		slevel string
		ilevel issue.SeverityLevel
	}{
		// npm
		{"critical",
			issue.SeverityLevelCritical,
		},
		{"high",
			issue.SeverityLevelHigh,
		},
		{"moderate",
			issue.SeverityLevelMedium,
		},
		{"low",
			issue.SeverityLevelLow,
		},
		{"unknown",
			issue.SeverityLevelUnknown,
		},
		{"undef",
			issue.SeverityLevelUndefined,
		},
		// yarn
		{"info",
			issue.SeverityLevelInfo,
		},
		{"low",
			issue.SeverityLevelLow,
		},
		{"moderate",
			issue.SeverityLevelMedium,
		},
		{"High",
			issue.SeverityLevelHigh,
		},
		{"critical",
			issue.SeverityLevelCritical,
		},
	} {
		t.Run(t.Name(), func(t *testing.T) {
			level := parseSeverityLevel(tc.slevel)
			require.Equal(t, tc.ilevel.String(), level.String())
		})
	}
}
