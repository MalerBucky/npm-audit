package metadata

import (
	"fmt"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

const (
	// AnalyzerVendor is the vendor/maintainer of the analyzer
	AnalyzerVendor = "GitLab"
	analyzerURL    = "https://gitlab.com/gitlab-org/security-products/analyzers/npm-audit/"

	// AnalyzerID is a short identifier of this analyzers
	AnalyzerID = "npm-audit"

	// ScannerName is the name of the used scanner
	ScannerName = AnalyzerID
	scannerID   = ScannerName

	// AnalyzerName identifies the scanner that generated the report
	AnalyzerName = AnalyzerID

	// nodeVersion provides the version number for nodejs
	npmVendor = "GitHub"
	npmURL    = "https://docs.npmjs.com/"

	// Type returns the type of the scan
	Type report.Category = report.CategoryDependencyScanning
)

var (
	// AnalyzerVersion is a placeholder value which the Dockerfile will dynamically
	// overwrite at build time with the most recent version from the CHANGELOG.md file
	AnalyzerVersion = "not-configured"

	// ScannerVersion is the semantic version of the scanner
	ScannerVersion = "not-configured"

	// IssueScanner describes the scanner used to find a vulnerability
	IssueScanner = report.Scanner{
		ID:   scannerID,
		Name: ScannerName,
	}

	// ReportAnalyzer provides meta-information about the analyzer
	ReportAnalyzer = report.AnalyzerDetails{
		ID:      AnalyzerID,
		Name:    AnalyzerName,
		Version: AnalyzerVersion,
		Vendor: report.Vendor{
			Name: AnalyzerVendor,
		},
	}

	// ReportScanner provides meta-information about the scanner
	ReportScanner = report.ScannerDetails{
		ID:      scannerID,
		Name:    scannerID,
		Version: ScannerVersion,
		Vendor: report.Vendor{
			Name: npmVendor,
		},
		URL: npmURL,
	}

	// AnalyzerUsage provides a one line usage string for the analyzer
	AnalyzerUsage = fmt.Sprintf("%s %s analyzer v%s", AnalyzerVendor, AnalyzerName, AnalyzerVersion)
)
