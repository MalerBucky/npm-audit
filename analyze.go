package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/logrusorgru/aurora"
	"github.com/urfave/cli/v2"
)

// PrependPath points to the path of the project under analysis
var PrependPath = ""

func pathExists(path string) bool {
	_, err := os.Stat(path)
	return err == nil
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	if canConnectToNpm() != nil {
		fmt.Println(aurora.Red("analyze: cannot connect to NPM"))
		// fail gracefully: return an empty string reader
		return ioutil.NopCloser(strings.NewReader("")), nil
	}

	if !depfileAvailable(path) {
		return nil, errors.New("No package manager identified")
	}

	pm, err := getPm(path)
	if err != nil {
		return nil, err
	}

	// check whether a lockfile was present before building the project
	os.Unsetenv(NpmAuditLockfileEnv)
	if pm.lockfilePresent(path) {
		os.Setenv(NpmAuditLockfileEnv, pm.lockfile())
	}

	err = pm.build(path)
	if err != nil {
		return nil, err
	}

	// we ignore the exit status because npm audit will return with
	// an exit status of 1 if it found a violation
	out, _ := pm.analyze(path)

	if len(out) <= 0 {
		err = fmt.Errorf("analyze failed for %s", pm.name())
	}

	log.Debugf("analyzer output: %s", string(out))

	return ioutil.NopCloser(bytes.NewReader(out)), err
}
