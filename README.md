This project's issue tracker has been disabled, if you wish to [create an issue or bug please follow these directions](/CONTRIBUTING.md#issue-tracker).

# NPM/YARN audit analyzer

Dependency Scanning for projects that are using `yarn audit` or `npm audit`.
This project wraps both the [npm audit](https://docs.npmjs.com/cli/audit) and
[yarn audit](https://classic.yarnpkg.com/en/docs/cli/audit/) analyzers which
both rely on the NPM API.

Note that `npm-audit` is an **unofficial** analyzer and not integrated into
GitLab itself. 

This analyzer is written in Go using the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Using npm-audit

npm-audit is available as a docker image. You can run try it locally by running
the following command:

``` bash
docker run --volume "$PWD/test/fixtures/npm-package-json:/tmp/project" \
    --env CI_PROJECT_DIR=/tmp/project \
    --env TOOL="yarn" \
    registry.gitlab.com/gitlab-org/security-products/analyzers/npm-audit:edge \
    /analyzer run
```

The `TOOL` variable can be set to either `yarn` or `npm` depending on which
package manager you prefer. The default setting is `npm`. 

In case you want to use npm-audit as an analyzer, in the CI pipeline of your
project, you can just add the job below to your `.gitlab-ci.yml`.

``` yaml
npm-audit:
  image: "registry.gitlab.com/gitlab-org/security-products/analyzers/npm-audit:1.4.0"
  stage: test
  variables:
    TOOL: npm
  script:
    - /analyzer run
  artifacts:
    reports:
      dependency_scanning: gl-dependency-scanning-report.json
    paths:
      - gl-dependency-scanning-report.json
```

You should be able to see the vulnerabilities in your Vulnerability
Dashboard.

## Credits 

We would like to thank the following authors very much for their valuable
contributions.

| Author  | MRs   |
| ------- | ----- |
| @tbonci | !3,!4 |

## Contributing

As `npm-audit` is an **unofficial** analyzer/community project, we cannot make
guarantees about the time frame in which we handle MRs/fix issues for
`npm-audit`. 

However, we :heart: community contributions; see
[`CONTRIBUTING.md`](CONTRIBUTING.md) for more details. We try to do our best to
handle them in a timely manner.

## License

This code is distributed under the MIT license; see the [LICENSE](LICENSE)
file.
