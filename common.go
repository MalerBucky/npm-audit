package main

import (
	"fmt"
	"net/http"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

const (
	// NpmAuditLockfileEnv is an environmentvariable to keep track of the lockfile (if present)
	NpmAuditLockfileEnv = "NPM_AUDIT_LOCKFILE"

	npmAPI       = "https://www.npmjs.com/advisories"
	npmLockfile  = "package-lock.json"
	npmDepFile   = "package.json"
	yarnLockFile = "yarn.lock"

	toolEnv = "TOOL"
)

// AuditReport captures the commonalities between NPM and Yarn audit reports
type AuditReport struct {
	Advisories map[string]Advisory
}

// Advisory represents a single advisory for both npm and yarn audit
type Advisory struct {
	Findings []struct {
		Version string
		Paths   []string
	}
	ID         int
	Title      string
	ModuleName string `json:"module_name"`
	Cves       []interface {
	}
	Severity string
	URL      string
}

func depfileAvailable(path string) bool {
	check := func(path string, lockfile string) bool {
		return pathExists(filepath.Join(path, lockfile)) || pathExists(filepath.Join(PrependPath, lockfile))
	}

	return check(path, npmDepFile) || check(path, yarnLockFile)
}

func canConnectToNpm() error {
	_, err := http.Get(npmAPI)
	if err != nil {
		return fmt.Errorf("cannot connect to NPM %w", err)
	}
	return nil
}

// see https://docs.npmjs.com/about-audit-reports#severity
// https://classic.yarnpkg.com/en/docs/cli/audit/
func parseSeverityLevel(s string) report.SeverityLevel {
	switch strings.ToLower(s) {
	case "moderate":
		return report.SeverityLevelMedium
	case "undef":
		return report.SeverityLevelUndefined
	default:
		return report.ParseSeverityLevel(s)
	}
}
