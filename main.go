package main

import (
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/npm-audit/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/npm-audit/plugin"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"

	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    "tool",
			EnvVars: []string{toolEnv},
			Usage:   "Path to denylist file.",
		},
	}
}

func main() {
	os.Setenv(command.EnvVarOptimizeReport, "false")

	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = metadata.AnalyzerVersion
	app.Authors = []*cli.Author{{Name: metadata.AnalyzerVendor}}
	app.Usage = metadata.AnalyzerUsage

	log.SetFormatter(&logutil.Formatter{Project: metadata.AnalyzerName})
	log.Info(metadata.AnalyzerUsage)

	app.Commands = command.NewCommands(command.Config{
		ArtifactName: command.ArtifactNameDependencyScanning,
		Match:        plugin.Match,
		Analyze:      analyze,
		Convert:      convert,
		AnalyzeFlags: analyzeFlags(),
		Scanner:      metadata.ReportScanner,
		Analyzer:     metadata.ReportAnalyzer,
		ScanType:     report.CategoryDependencyScanning,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
