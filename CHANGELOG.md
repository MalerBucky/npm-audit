# npm-audit analyzer (npm|yarn) audit changelog

## v1.6.1
Upgrade golang.org/x/crypto dependency (!20)

## v1.6.0
Upgrade to Dependency Scanning report schema v14.0.4 (!18)

## v1.5.0
Upgrade Docker image to Go 1.17 (!17)

## v1.4.1
Ensure dependency_files.dependencies is an empty array (!16)

## v1.4.0
Update security report format (!14)

## v1.3.0
Infer version in Docker build

## v1.2.0
Addition of integration tests and documentation.

## v1.1.0
Use Node 14 (LTS version)

## v1.0.0
Initial release
