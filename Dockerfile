FROM golang:1.17-alpine AS analyzer-build
COPY . /build
WORKDIR /build
RUN go build -o /analyzer
ARG NPM_VERSION

# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
    PATH_TO_MODULE=`go list -m` && \
    go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION' -X '$PATH_TO_MODULE/metadata.ScannerVersion=$NPM_VERSION'" -o /analyzer

FROM node:14-alpine
ARG NPM_VERSION
# install git for project with git-sourced dependencies
RUN apk update && apk add --no-cache git npm=$NPM_VERSION

# temporary workaround for
# https://github.com/nodejs/docker-node/issues/813#issuecomment-407339011
RUN npm config set unsafe-perm true

COPY --from=analyzer-build /analyzer /
WORKDIR "/"
ENTRYPOINT []
CMD ["/analyzer", "run"]
